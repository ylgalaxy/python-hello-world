# -- coding: utf-8 --


from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return '恭喜您！！！你已经将这个Python Hello World示例项目成功运行\n。接下来，您可以尝试将您自己的后端代码运行起来，无论是java、Python，还是使用Go或其他编程语言写的\n。 请开始您的Methodot之旅，需要协助请随时联系我们。'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
